/*
Forward Euler explicit method of solving diffusion equation with boundaries v(0,t) = v(d=1,t) = 0.
The actual boundary value was u(0,t) = 1, so the steady-state solution is u_s(x) = 1 - x. 
The diffusion starts at time t = 0, and u(x,t<0) = 0. Whence v(x,t<0) = x - 1.  

	the trick is to solve v(x,t), and superpose u_s(x) onto solution

Comparison parameters:
dx = 0.1
dt = 0.001
t from 0 to 0.1

saving only snapshots at 
	t1 = 0.03
and
	t2 = 0.09
*/

#include <iostream>
#include <fstream>
#include <armadillo>

using namespace std;
using namespace arma;

void initialise(int*,int*, double*);

int main()
{


							//define variables and constants
	int N; 					//grid points along x without counting two boundary points
	int tsteps; 			//grid points along t
	double alpha; 			//dt/dx**2
	double totaltime;
	double dt;
	double dx; 				// dx = 1/(N+1)
	int i; 					// dx counter
	int t;					// dt counter
	double b;				// diagonal value of tridiagonal matrix


							//call for initial specifications
	initialise(&N, &tsteps, &totaltime);
	dx = 1./float(N+1); 	//total length of system is 1
	dt = totaltime/float(tsteps);
	alpha = dt/(pow(dx,2));
	cout << alpha << endl;	//check that alpha is leq(.5) for stability 
	b = 1 - 2.*alpha;
	
	vec v = zeros<vec>(N+2);
	vec vnew = zeros<vec>(N+2);
	vec v_s = zeros<vec>(N+2);
							//let v take on v_0
	for (i = 0; i <= N+1; i++) {
		v_s(i) = 1. - i*dx; 
		v(i) = i*dx - 1.;
	}
	v(0) = 0;				//rigidly enforce v(0) = v(1) = 0 at all times
	v(N+1) = 0;
							//now we want to solve tridiagonal matrix with alpha on both sides
							//and 1-2*alpha along diagonal
							//we want to write out a density profile for each timestep
	ofstream outfile;
	outfile.open("fe12.dat");
	//outfile << dx << endl;	//output parameter data first
	//outfile << N << endl;
	//outfile << dt << endl;
	//outfile << tsteps << endl;
	//outfile << totaltime << endl;
	//outfile << alpha << endl;
	//outfile << v_s << endl;
	//outfile << v << endl;

	for (t = 1; t < tsteps; t++) {
							// 1 timestep
		for (i = 1; i <= N; i++) {
							// solve along x for the timestep
			vnew(i) = alpha*v(i-1) + (1 - 2*alpha)*v(i) + alpha*v(i+1);
		}
							// take a snapshot of density distribution
		v = vnew;
		v(0) = 0;				//rigidly enforce v(0) = v(1) = 0 at all times
		v(N+1) = 0;
		if (t == 5) {		//print t = 0.01
			outfile << v + v_s << endl;
		}
		if (t == 10) {		//print t = 0.05
			outfile << v + v_s << endl;
		}	
	}
	
	outfile.close();

	return 0;
}

void initialise (int* N, int* tsteps, double* totaltime) {
	cout << "N is number of grid points along length, not including endpoints. N = " << endl;
	cin >> *N;
	cout << "tsteps is number of grid points along timeaxis. tsteps = " << endl;
	cin >> *tsteps;
	cout << "totaltime is the total time of experiment. totaltime = " << endl;
	cin >> *totaltime;
}
