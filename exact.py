from scitools.std import *

def fourier(N,x,t):
	total = 0
	term = 0
	n = 1
	while (n <= N):
		term = (1./n) * sin(n*pi*x) * exp(-t*(n*pi)**2)
		total += term
		n += 1
	return -2*total/pi

xsteps = 101
tsteps = 21
x = linspace(0,1,xsteps)
t = linspace(0,0.1,tsteps)

#exact solutions
u0 = zeros(xsteps)
u1 = zeros(xsteps)

#forward euler
feu0 = zeros(xsteps)
feu1 = zeros(xsteps)

#backward euler
beu0 = zeros(xsteps)
beu1 = zeros(xsteps)

#crank-nicolson
#cnu0 = zeros(11)
cnu0 = zeros(xsteps)
cnu1 = zeros(xsteps)

#save solutions for exact
for j in range(len(u1)):
	u0[j] = 1 - x[j] + fourier(50,x[j],t[5])
	u1[j] = 1 - x[j] + fourier(50,x[j],t[10])


#save solutions for forward euler
fe = open('fe12.dat', 'r')
for i in range(xsteps):
	feu0[i] = float(fe.readline())
fe.readline()	
for i in range(xsteps):
	feu1[i] = float(fe.readline())
fe.close()

#save solutions for backward euler
be = open('be12.dat', 'r')
for i in range(xsteps):
	beu0[i] = float(be.readline())
be.readline()	
for i in range(xsteps):
	beu1[i] = float(be.readline())

be.close()

#save solutions for crank-nicolson
cn = open('cn12.dat', 'r')
for i in range(xsteps):
	cnu0[i] = float(cn.readline())
cn.readline()
for i in range(xsteps):
	cnu1[i] = float(cn.readline())
cn.close()

plot(x,u0,'-')
hold('on')
plot(x,u1,'-')
plot(x,feu0,'o')
plot(x,feu1,'o')
plot(x,beu0,'x')
plot(x,beu1,'x')
plot(x,cnu0,'--')
plot(x,cnu1,'--')
xlabel('x')
ylabel('u')
axis([-0.1,1.1,-0.1,1.1])
legend('Exact t = 0.025','Exact t = 0.05','Forward t = 0.025','Forward Euler t = 0.05','Backward t = 0.025','Backward t = 0.05','Crank t = 0.025','Crank t = 0.05')
title('100 xsteps, 20 tsteps, t form 0 to 0.1')
savefig('middle.png')

