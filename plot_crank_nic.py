from scitools.std import *
import glob, os
for filename in glob.glob('cnd*.png'):
	os.remove(filename)

f = open("diff_crank_nic.dat", 'r')
dx = float(f.readline())
N = int(f.readline())
dt = float(f.readline())
tsteps = int(f.readline())
totaltime = float(f.readline())
alpha = float(f.readline())
x = linspace(0,1,N+2)
t = linspace(0,totaltime,tsteps)
v_s = zeros(N+2)
u = zeros(N+2)
for i in range(N+2):
	v_s[i] = float(f.readline())
f.readline() 								#skip empty line


for s in range(tsteps):	
	for i in range(N+2):
		u[i] = float(f.readline()) + v_s[i]
	f.readline()
	plot(x,u)
	xlabel('x')
	ylabel('u')
	axis([-0.1,1.1,-0.1,1.1])
	legend('u(x,t=%f' %t[s])
	title('Crank-Nicolson Diffusion')
	savefig('cnd%d.png' %s)

movie('cnd*.png', encoder='convert', fps=6, output_file='cndmovie.gif')

f.close()
