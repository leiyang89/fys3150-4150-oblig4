\documentclass[a4wide,12pt]{article}

\usepackage{verbatim}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{a4wide}
\usepackage{color}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[T1]{fontenc}
\usepackage{cite} % [2,3,4] --> [2--4]
\usepackage{shadow}
\usepackage{hyperref}

\begin{document}
\title{Project 4: 1 Dimensional Diffusion Equation
: source files to be found on bitbucket under leiyang89}
\author{Lei Yang}
\date{10.11.2013}
\maketitle
\section*{Abstract}
Diffusion-driven signal transport between neurons in the brain is simulated as
a 1-dimensional partial differential equation using
three algorithms: Forward Euler, Backward Euler, and Crank-Nicolson, and compared
to the exact solution of the diffusion equation. The three methods are found
to be applicable at different sets of step sizes for distance and time, and 
an aproximate domain for each method is suggested for solutions of open-form problems.
\section*{Introduction}
Signal transport between neurons in the brain is mainly driven by density gradients.
Synaptic vesicles carry transmitter molecules with information from the
axon terminal to the synaptic cleft, where the molecules must travel through
the synaptic cleft to the postsynaptic membrane, where they are absorbed,
and the information received.
When vesicles filled with neurotransmitter molecules merge with the 
presynaptic membrane and release neurotransmitters into the synaptic cleft, 
we can assume that the synaptic cleft is 'empty', or at least has a much lower
density of molecules. The transmitter molecules therefore diffuse across
the cleft to the postsynaptic membrane, where they are absorbed. 
The absorption drives the continued diffusion, as a lower density is constantly
created at the receiving side. 
If we consider a timescale where the signal is in the process of being sent,
we can neglect signal saturation on both ends, and therefore
have a constant source and an ideal sink. 
If the length of the distance
between the membranes is small compared to the contact area with the vesicles, 
the diffusion along most of the synaptic cleft
can be approximated as 1 dimensional.  

The density of transmitter molecules
will then start at the initial position with a constant density, propagating
towards the postsynaptic membrane where it is absorbed. Over time,
the density distribution along the cleft 
reaches a steady-state scenario, like sand poured over a period of time 
eventually makes a constant angle with the ground. 
The steady-state of a constant source and 
a receiver with infinite capacity must therefore be linear, as any fluctuation away from
the linear case are smoothed out over time by the signal receiver. 

The purpose of this exercise is then to simulate the process that occurs between
the time of initial contact between synapses and membrane, and the time
when steady-state has been reached. 

\section*{1-dimensional Diffusion Equation}
1-dimensional diffusion is described by: 
%
\begin{equation}
\frac{\partial u}{\partial t} = D \frac{\partial^2 u}{\partial x^2}.
\label{eq:diffusion_eq_1D}
\end{equation}\newline
where u is the concentration of signal molecules, and
$D$ is the diffusion coefficient of the neurotransmitter in this
particular environment (solvent in synaptic cleft).
The problem is simplified by normalizing to D = 1. 

The simplest way to solve the diffusion equation is with Dirichlet boundary
conditions u = 0 at both ends of the cleft. Since the steady-state solution 
 takes on the appropriate boundary values prescribed
for the problem, one could superpose an additional density distribution also
obeying the diffusion equation, but with boundary conditions u = 0 at both ends. 
The superposed distribution v(x,t) must cancel the steady state solution in such
a way that at t = 0, set as the time right before contact between vesicle and cleft,
the entire distribution along the synaptic cleft is 0. We therefore seek to solve: 
\begin{equation}
\frac{\partial v}{\partial t} = \frac{\partial^2 v}{\partial x^2}, 
\end{equation}
where
\begin{equation}
v(x,t) = u(x,t) - u_s(x),  
\end{equation}
with $v_s$ as the steady-state solution, and u(x,t) as the actual density 
distribution. This v(x,t) satisfies v = 0 at both ends of the cleft. 

The steady-state solution is given by removing the time-dependence of the diffusion
equation:  
\begin{equation}
\frac{\partial^2 u_s}{\partial x^2} = 0. 
\end{equation}
Thus,
\begin{equation}
u_s = Ax + B, 
\end{equation}
which due to the boundary conditions becomes
\begin{equation}
u_s = 1 - x. 
\end{equation}

v(x,t) should be solved as a separable differential equation. Setting
separation constant $-k^2$, and letting
\begin{equation}
v(x,t) = F(x)T(t),
\end{equation} 
the diffusion equation becomes 
\begin{equation}
\frac{\partial T}{T\partial t} = \frac{\partial^2 F}{F\partial x^2} = -k^2. 
\end{equation} 
The equation for T is easy to solve, and we get: 
\begin{equation}
T(t) = const \times e^{-k^2 t}.
\end{equation} 
The constant can be absorbed into F(x), thus:
\begin{equation}
v(x,t) = F(x) \times e^{-k^2 t}.
\end{equation} 
We have no reason to assume a polynomial solution to F(x), and therefore
wish to express v(x,t) as a superposition of Fourier terms. The superposition
will then serve as a general solution. 

If we set the ends of the synaptic cleft at x = 0 and x = 1, our boundary conditions
for v(x,t) are v(0,t>0) = v(1,t>0) = 0. Since this is a function that disappears
at the edges, only $sin(n\pi x)$ terms need be considered for the fourier expansion,
with $k = n\pi$.  
Since the initial condition must exactly cancel $u_s$, we have that:
\begin{equation}
\sum\limits_{i=1}^n F(x,n) = x - 1,
\end{equation} 
or 
\begin{equation}
\sum\limits_{i=1}^n b(n)sin(n\pi x) = x - 1.
\end{equation} 
b(n) is found using: 
\begin{equation}
b(n) = 2\int_0^1 (x-1)sin(n\pi x) dx = -\frac{2}{n\pi}.
\end{equation} 
This solves for the density distribution v(x,t):
\begin{equation}
v(x,t) = -\frac{2}{\pi}\sum\limits_{i=1}^n \frac{sin(n\pi x)}{n}e^{-(n\pi)^2 t }
\end{equation} 
Superposition against the steady-state solution thus yields the exact closed-form
density distribution of transmitter molecules: 
\begin{equation}
u(x,t) = 1 - x -\frac{2}{\pi}\sum\limits_{i=1}^n \frac{sin(n\pi x)}{n}e^{-(n\pi)^2 t}.
\end{equation} 
We can use the exact solution to test our algorithms for the diffusion equation.


\section*{3 Algorithms for Solving the Diffusion Equation}
Three basic schemes that can be used to solve partial differential equations
are the Forward Euler, Backward Euler, and Crank-Nicolson algorithms. 
Forward Euler is explicit, and thus has stability issues for large numbers of 
distance-steps as compared to time-steps. On the other hand, the Backwards Euler
method is implicit, and has no stability issues. It does have accuracy issues for
small numbers of distance-steps, since it relies on matrix solving algorithms
that may lead to loss of numerical precision for small matrices. Thus even though
the truncation (local) error is of the same order, different numbers of steps
may lead to different global errors. 
The Crank-Nicolson
is an implicit algorithm that combines features of both Eulers. It is always stable,
and its truncation error is one order better than the Euler methods.  

Below is a synopsis of the three schemes, taken from the problem handout:
\begin{enumerate}
\item The explicit forward Euler algorithm with discretized versions of time given by a forward formula and
a centered difference in space resulting in
 \[
u_t\approx \frac{u(x_i,t_j+\Delta t)-u(x_i,t_j)}{\Delta t}
\]
and
\[
u_{xx}\approx \frac{u(x_i+\Delta x,t_j)-2u(x_i,t_j)+u(x_i-\Delta x,t_j)}{\Delta x^2}.
\]
\item The implicit Backward Euler with
 \[
u_t\approx \frac{u(x_i,t_j)-u(x_i,t_j-\Delta t)}{\Delta t}
\]
and
\[
u_{xx}\approx \frac{u(x_i+\Delta x,t_j)-2u(x_i,t_j)+u(x_i-\Delta x,t_j)}{\Delta x^2},
\]
\item The implicit Crank-Nicolson scheme with 
a time-centered scheme at $(x,t+\Delta t/2)$
 \[
u_t\approx \frac{u(x_i,t_j+\Delta t)-u(x_i,t_j)}{\Delta t}.
\]
The corresponding spatial second-order derivative reads
\[
u_{xx}\approx \frac{1}{2}\left(\frac{u(x_i+\Delta x,t_j)-2u(x_i,t_j)+u(x_i-\Delta x,t_j)}{\Delta x^2}+\right.
\]
\[
\left. \frac{u(x_i+\Delta x,t_j+\Delta t)-2u(x_i,t_j+\Delta t)+u(x_i-\Delta x,t_j+\Delta t)}{\Delta x^2}
\right).
\] 
\end{enumerate}
For the computational version of the problem, separation into $u(x,t) = u_s(x) + v(x,t)$
is also the best way to proceed, as the boundary conditions 
reduce the matrices for implicit solutions to tridiagonal form, which can be
handled by the program written for Problem 1 at the start of the semester. When
one wishes to compare a specific snapshot, one can then easily superpose the density
distribution from $u_s$. 
In all cases, the initial distribution has been set to $x - 1$, except at x=0,
where boundary conditions dictate v = 0.  
\begin{enumerate}
\item The Forward Euler may be expressed as a matrix equation. Remembering that
i enumerates positional steps, and j enumerates timesteps, and
defining $\alpha = \frac{\Delta t}{\Delta x^2}$:
 \begin{equation}
u(i,j+1) - u(i,j) = \alpha [u(i+1,j) - 2u(i,j) + u(i-1,j)].
  \end{equation}
Moving all terms of one timestep to one side of the equation, we can express 
the equation as a tridiagonal matrix, since the dependency of $u(i,j+1)$ 
on $u(i,j)$  is
never beyond one position-step away: 
\begin{equation}
    {\bf A} = \left(\begin{array}{cccccc}
                           1-2\alpha& \alpha & 0 &\dots   & \dots &\dots \\
                           \alpha & 1-2\alpha & \alpha &\dots &\dots &\dots \\
                           & \alpha & 1-2\alpha & \alpha & \dots & \dots \\
                           & \dots   & \dots &\dots   &\dots & \dots \\
                           &   &  &\alpha  &1-2\alpha& \alpha \\
                           &    &  &   &\alpha & 1-2\alpha \\
                      \end{array} \right)\left(\begin{array}{c}
                           v_j(1)\\
                           v_j(2)\\
                           \dots \\
                          \dots  \\
                          \dots \\
                           v_j(N-1)\\
                      \end{array} \right)
  =\left(\begin{array}{c}
                           v_{j+1}(1)\\
                           v_{j+1}(2)\\
                           \dots \\
                           \dots \\
                          \dots \\
                            v_{j+1}(N-1)\\
                      \end{array} \right).
\end{equation} 
if v(0) and v(N) mark the boundaries.  

The algorithm is to straightforwardly loop the matrix multiplication
over each timestep. Snapshots may then be written to text files within the loop.
I rigidly enforce boundary conditions every timestep
out of paranoia, but this should not be necessary. 
\begin{lstlisting}[title={Forward Euler}]
for (t = 1; t < tsteps; t++) {
						// per timestep
	for (i = 1; i <= N; i++) {
						// solve along x 
		vnew(i)=alpha*v(i-1)+(1-2*alpha)*v(i)+alpha*v(i+1);
	}
	v = vnew;
	v(0) = 0;				//rigidly enforce 
						//v(0) = v(1) = 0
	v(N+1) = 0;
	if (t == 5) {				//snapshot at timestep
		outfile << v + v_s << endl;
	}
	if (t == 10) {				//snapshot at timestep
		outfile << v + v_s << endl;
	}	
}
\end{lstlisting} 
\item Backward Euler is expressed as a matrix through the same process
as for Forward Euler. It is expressed: 
\begin{equation}
    {\bf A} = \left(\begin{array}{cccccc}
                           1+2\alpha& \alpha & 0 &\dots   & \dots &\dots \\
                           \alpha & 1+2\alpha & \alpha &\dots &\dots &\dots \\
                           & \alpha & 1+2\alpha & \alpha & \dots & \dots \\
                           & \dots   & \dots &\dots   &\dots & \dots \\
                           &   &  &\alpha  &1+2\alpha& \alpha \\
                           &    &  &   &\alpha & 1+2\alpha \\
                      \end{array} \right)\left(\begin{array}{c}
                           v_{j}(1)\\
                           v_{j}(2)\\
                           \dots \\
                           \dots \\
                          \dots \\
                            v_{j}(N-1)\\
                      \end{array} \right)
  =\left(\begin{array}{c}
                           v_{j-1}(1)\\
                           v_{j-1}(2)\\
                           \dots \\
                          \dots  \\
                          \dots \\
                           v_{j-1}(N-1)\\
                      \end{array} \right).
\end{equation} 
After setting the initial conditions, this matrix uses the tridiagonal
solution algorithm from Project 1 to forward substitute the diagonal and 
the present timestep, before it backward substitutes the solutions into the next 
timestep. The main pitfall here is when one forgets to reset the diagonal for
every timestep. 
\begin{lstlisting}[title={Backward Euler}]
for (t = 1; t <= tsteps; t++) {
	//Forward subst
	for (i = 2; i <= N; i++) {						
		b(i) = b(i) - (pow(a,2))/b(i-1); 
		v(i) = v(i) - a*v(i-1)/b(i-1); 
	}

	//Backward and solve
	vnew(N-1) = v(N-1)/b(N-1);
	j = N-1;
	
	while (j > 1) {
		vnew(j-1) = (v(j-1) - a*vnew(j))/b(j-1);
		j--;
	}
	vnew(0) = 0.;
	vnew(N+1) = 0.;
	v = vnew;
	if (t == 5) {		//snapshot
		outfile << v  + v_s << endl;
	}
	if (t == 10) {		//snapshot
		outfile << v_s + v << endl;
	}
	b.fill(1 + 2.*alpha);
	vnew.fill(0);
	}
\end{lstlisting} 
\item Finally, the Crank-Nicolson is formulated as 
\begin{equation}
(2I + \alpha B) V_j = (2I - \alpha B) V_{j-1},
\end{equation}
where
\begin{equation}
    {B} = \left(\begin{array}{cccccc}
                           2& -1 & 0 &\dots   & \dots &\dots \\
                          -1 & 2 & -1 &\dots &\dots &\dots \\
                           & -1 & 2 & -1 & \dots & \dots \\
                           & \dots   & \dots &\dots   &\dots & \dots \\
                           &   &  &-1  &2& -1 \\
                           &    &  &   &-1 & 2\\
                      \end{array} \right)
\end{equation} 
and I is the identity matrix of corresponding dimensionality. 

The trick is to solve the right-hand side before using the tridiagonal solver
to solve for $V_j$.

The algorithm:
\begin{lstlisting}[title={Crank-Nicolson}]
for (t = 1; t < tsteps; t++) {
	//calculate v for use in tridiagonal solver
	for (i=1; i <= N; i++) {
		vnew(i) = alpha*v(i-1) + (2-2*alpha)*v(i) + alpha*v(i+1);
	}
	v = vnew;
	//Forward subst
	for (i = 2; i <= N; i++) {						
		b(i) = b(i) - (pow(a,2))/b(i-1); 
		v(i) = v(i) - a*v(i-1)/b(i-1); 
	}

	//Backward and solve
	vnew(N-1) = v(N-1)/b(N-1);
	j = N-1;
	while (j > 1) {
		vnew(j-1) = (v(j-1) - a*vnew(j))/b(j-1);
		j--;
	}
	vnew(0) = 0;
	vnew(N+1) = 0;
	v = vnew;
	if (t == 5) {
		outfile << v + v_s << endl;
	}
	if (t == 10) {		//print t = 0.3
		outfile << v + v_s << endl;
	}
	b.fill(2 + 2.*alpha);
}
\end{lstlisting} 
As can be seen, the algorithm is just the tridiagonal solver,
with an added algorithm of precalculating $(2I - \alpha B)V_{j-1}$.

These algorithms should be compared to the exact solution, here rendered in
python as part of a plotting program:  
\begin{lstlisting}[title={Exact}]
from scitools.std import *

def fourier(N,x,t):
	total = 0
	term = 0
	n = 1
	while (n <= N):
		term = (1./n) * sin(n*pi*x) * exp(-t*(n*pi)**2)
		total += term
		n += 1
	return -2*total/pi

for j in range(len(u1)):
	u0[j] = 1 - x[j] + fourier(50,x[j],t[5])
	u1[j] = 1 - x[j] + fourier(50,x[j],t[10])

\end{lstlisting} 
\end{enumerate}

\section*{Discussion}
The truncation errors of the Forward Euler algorithm are $O(\Delta x^2)$ and $O(\Delta t).$
Its stability requirement is $\alpha \leq \frac{1}{2} $. Thus it is useful for
small numbers of positions (small matrices), and many timesteps. This
might be useful when we mainly want to see the time-evolution of the system,
but for most purposes we actually want a better resolution, i.e. a smooth curve,
to compare the problem with an exact solution. Below is a figure of the various
algorithms plotted against the exact solution at $\alpha = \frac{1}{2} $, the
boundary of Forward Euler stability. (The 1000 xsteps in the title is a typo, this plot
is for 10 xsteps, as can be seen from the number of dots for Forward Euler.)
\begin{center}
 \includegraphics{./forwardedgewins.png}
 % forwardedgewins.png: 812x612 pixel, 100dpi, 20.62x15.54 cm, bb=0 0 585 441
\end{center}
We notice that both Backward Euler and Crank-Nicolson are completely
inaccurate in this domain. The reason is the small matrix size mentioned before.


Below is a figure for the opposite case, where the number of positional steps
is so large that the tridiagonal solver is accurate. In this case, the Backward
Euler is accurate, while the Forward Euler has blown up. Suprisingly, the Crank
-Nicolson has blown up too, due to loss of accuracy in the 'Forward Euler' term 
within each timestep, where we precalculated the previous v to be inserted
into the tridiagonal solver. 
\begin{center}
 \includegraphics{./backwardwins.png}
 % backwardwins.png: 812x612 pixel, 100dpi, 20.62x15.54 cm, bb=0 0 585 441
\end{center}

Finally, a figure of the middling number of 100 xsteps is added, to show how the 
Crank-Nicolson algorithm becomes more accurate as tsteps and xsteps are more
approxmately equal to each other. With the smaller local truncation error for $\Delta t$,
Crank-Nicolson will be most accurate when x and t have the same resolution. 
\begin{center}
 \includegraphics{./middle.png}
 % middle.png: 812x612 pixel, 100dpi, 20.62x15.54 cm, bb=0 0 585 441
\end{center}
There is a possibility that some of the inaccuracy in the tridiagonal solver
is due to problems of numerical precision, but simply capping small values of
matrix elements to 0 would introduce other problems, as the simulation considered
often has arbitrarily small matrix element values, defined by the step sizes.  


All three algorithms solve the specific diffusion equation for the problem
considered, but they are specialised for different resolutions. 
If we wanted to make a simple demonstrative video of time-progression, 
Forward Euler would be a decent choice, while if we want to look at static pictures
of the diffusion at specific times, backward Euler offers the best resolution.
Crank-Nicolson would be the best overall for analysing the problem for equal
steps of the two variables, i.e. if we wanted to analyse the problem more analytically.
Of course, when a closed-form solution exists, it is naturally better, but the
hope is that the implementation of these methods may be extended to differential equations 
without exact closed-form solutions, in which case we have a picture of stability
and error that would otherwise be untestable. 

\end{document}








