/*
Crank-Nicolson implicit method of solving diffusion equation with boundaries v(0,t) = v(d=1,t) = 0.
The actual boundary value was u(0,t) = 1, so the steady-state solution is u_s(x) = 1 - x. 
The diffusion starts at time t = 0, and u(x,t<0) = 0. Whence v(x,t=0) = x - 1, except at v(0,0) = 0, 
as the boundaries should be rigidly enforced.   

	the trick is to solve v(x,t), and superpose u_s(x) onto solution
	also, the tridiagonal solver from project 1 should be used

For Crank-Nicolson: 
1) V_j-1 <- (2I - alpha*B)V_j-1
2) (2I + alpha*B)V_j -> V_j        ---      tridiagonal solver... 
*/

#include <iostream>
#include <fstream>
#include <armadillo>

using namespace std;
using namespace arma;

void initialise(int*,int*, double*);

int main()
{


							//define variables and constants
	int N; 					//grid points along x without counting two boundary points
	int tsteps; 			//grid points along t
	double alpha; 			//dt/dx**2
	double totaltime;
	double dt;
	double dx; 				// dx = 1/(N+1)
	int i; 					// dx counter
	int t;					// dt counter
	int a;					// non-diagonal elements of matrix A
	int j;

							//call for initial specifications
	initialise(&N, &tsteps, &totaltime);
	dx = 1./float(N+1); 	//total length of system is 1
	dt = totaltime/float(tsteps);
	alpha = dt/(pow(dx,2));
	cout << alpha << endl;	//check that alpha is leq(.5) for stability 
	a = -alpha;
							//now we want to solve Av_j = v_j-1 with v_j as unknown
	
	vec v = zeros<vec>(N+2);
	vec vnew = zeros<vec>(N+2);
	vec v_s = zeros<vec>(N+2);
	vec b =	zeros<vec>(N+2);			// diagonal value of tridiagonal matrix
	b.fill(2 + 2.*alpha);
							//let v take on v_0
	for (i = 1; i <= N; i++) {
		v_s(i) = 1. - i*dx; 
		v(i) = i*dx - 1.;
	}
	v_s(0) = 1.;
	v_s(N+1) = 0;
	v(0) = 0;				//rigidly enforce boundary conditions
	v(N+1) = 0;

							//now we want to solve tridiagonal matrix with -alpha on both sides
							//and 2+2*alpha along diagonal
							//we want to write out a density profile for each timestep
	ofstream outfile;
	outfile.open("cn12.dat");
	//outfile << dx << endl;	//output parameter data first
	//outfile << N << endl;
	//outfile << dt << endl;
	//outfile << tsteps << endl;
	//outfile << totaltime << endl;
	//outfile << alpha << endl;
	//outfile << v_s << endl;
	//outfile << v << endl;

	//loop this part over time
	for (t = 1; t < tsteps; t++) {
		//calculate v for use in tridiagonal solver
		for (i=1; i <= N; i++) {
			vnew(i) = alpha*v(i-1) + (2-2*alpha)*v(i) + alpha*v(i+1);
		}
		v = vnew;

		//Forward subst
		for (i = 2; i <= N; i++) {					//changed i = 2 to i = 3, N to N+1	also below		
			b(i) = b(i) - (pow(a,2))/b(i-1); 
			v(i) = v(i) - a*v(i-1)/b(i-1); 
		}
	
		//Backward and solve
		vnew(N-1) = v(N-1)/b(N-1);
		j = N-1;
		while (j > 1) {
			vnew(j-1) = (v(j-1) - a*vnew(j))/b(j-1);
			j--;
		}
		vnew(0) = 0;
		vnew(N+1) = 0;
		v = vnew;
		if (t == 5) {
			outfile << v + v_s << endl;
		}
		if (t == 10) {		//print t = 0.3
			outfile << v + v_s << endl;
		}
		b.fill(2 + 2.*alpha);
		//outfile << v << endl;
	}
	outfile.close();

	return 0;
}

void initialise (int* N, int* tsteps, double* totaltime) {
	cout << "N is number of grid points along length, not including endpoints. N = " << endl;
	cin >> *N;
	cout << "tsteps is number of grid points along timeaxis. tsteps = " << endl;
	cin >> *tsteps;
	cout << "totaltime is the total time of experiment. totaltime = " << endl;
	cin >> *totaltime;
}
