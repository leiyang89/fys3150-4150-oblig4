#include <iostream>
#include <fstream>
#include <armadillo>

using namespace std;
using namespace arma;

int main()
{
	int i;
	int j;
	double alpha = 0.2;
	mat A = zeros<mat>(10,10);
	for (i = 0; i < 10; i++) {
		for (j=0;j<10;j++) {
			if (i==j) {
				A(i,j) = 1 - 2*alpha;
			}
			if (i == (j-1)) {
				A(i,j) = -alpha;
			}
			if (i == (j+1)) {
				A(i,j) = -alpha;
			}
		}
	}
	mat B = symmatu(A);
	mat X = inv(B);
	cout << B << endl;
	return 0;
}
